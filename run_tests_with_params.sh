#!/bin/bash

MVN_CLI_OPT=""

while getopts t:e:h flag; do
  case "${flag}" in
  t) tags=${OPTARG} ;;
  e) environment=${OPTARG} ;;
  h) usage ;;
  *) usage ;;
  esac
done

usage() {
  echo
  echo " IMPLEMENT ME!"
  exit 0
}

if [ -n "${tags}" ]; then
  MVN_CLI_OPT="${MVN_CLI_OPT} -Dcucumber.options=\"--tags=${tags}\""
fi

if [ -n "${environment}" ]; then
  MVN_CLI_OPT="${MVN_CLI_OPT} -Denvironment=${environment}"
fi

echo mvn clean verify ${MVN_CLI_OPT}
mvn clean verify ${MVN_CLI_OPT}
