# Serenity - cucumber - rest assured - java demo project

## Synopsis

This project is using Java, Maven, Serenity, Rest Assured and Cucumber to demonstrate how it can intertwine.

## Make
After cloning the repo to a machine, to make this project work there the following options:
- **_Run it locally with Maven + Java:_**
    ```
    mvn -s "/path/to/apache-maven-3.6.3/conf/settings.xml" -Dmaven.repo.local="/path/to/apache-maven-3.6.3/repository/serenity-test" clean verify -Denvironment=prod -Dcucumber.options="--tags=@positive-scenario" 
    ```
    - Where (both are optional):
        - environment is dev or prod, default value is prod;
        - cucumber.options="--tags=@positive-scenario or @negative-scenario" are the cucumber tags written in cucumber language

## Create new tests
- **_Append/Create new feature file in resources/features_**
- **_Define step definitions classes in the package ro.agiletesting.serenitytest.glue.stepdefinitions_**
- **_Generic Response assertions should be created in HttpAssertionsStepDefinition_**
- **_Specific request/response steps should be created in a new or existing classes if they're from the same domain_**

## Reports
Serenity reports can be found at:
- Full Report: file:///${project.build.directory}/site/serenity/index.html
- Single Page HTML Summary: file:///${project.build.directory}/site/serenity/serenity-summary.html
- Full Report As React Based Single Page Application: file:///${project.build.directory}/site/serenity/navigator/index.html
