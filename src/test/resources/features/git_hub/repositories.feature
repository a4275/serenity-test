Feature: Test interaction with GitHub API repositories and branches

  @positive-scenario
  Scenario: Given a correct user it is able to retrieve all the repositories
    Given the user "octocat"
    When he retrieves all his repositories
    Then he should find at least some of the following repositories:
      | Hello-World |
      | linguist    |

  @negative-scenario
  Scenario: Given an incorrect user it gets an error message
    Given the user "userDoesntExist"
    When he retrieves all his repositories
    Then gets the following key "message" and value "Not Found" in the response body

  @negative-scenario
  Scenario: Given a correct user with no repositories it gets an empty list
    Given the user "octocat23"
    When he retrieves all his repositories
    Then gets an empty array

  @positive-scenario
  Scenario Outline: Given a correct user and repositories it is able to retrieve all branches
    Given the user "octocat"
    When he retrieves repository "<repositoryName>" branches
    Then he should find at least the following branch "<branchName>"
    Examples:
      | repositoryName | branchName |
      | Hello-World    | master     |
      | linguist       | bower      |

  @negative-scenario
  Scenario: Given a correct user with incorrect repository it gets 404 response
    Given the user "octocat"
    When he retrieves repository "repositoryNameNotFound" branches
    Then gets the following error code 404