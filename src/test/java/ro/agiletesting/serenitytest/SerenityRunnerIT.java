package ro.agiletesting.serenitytest;

import io.cucumber.junit.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.runner.RunWith;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(
		features = "src/test/resources/features",
		glue = "ro.agiletesting.serenitytest.glue")
public class SerenityRunnerIT {

}
