package ro.agiletesting.serenitytest.glue.stepdefinitions;

import io.cucumber.java.en.Then;
import net.serenitybdd.rest.SerenityRest;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

public class HttpAssertionsStepDefinition {
	@Then("gets an empty array")
	public void heShouldFindAnEmptyListOfRepositories() {
		String response = SerenityRest.lastResponse().thenReturn().body().asString();
		assertThat(response, equalTo("[]"));
	}

	@Then("gets the following error code {int}")
	public void getsTheFollowingErrorCode(int code) {
		int statusCode = SerenityRest.lastResponse().thenReturn().getStatusCode();
		assertThat(statusCode, equalTo(code));
	}

	@Then("gets the following key {string} and value {string} in the response body")
	public void getsTheFollowingKeyAndValueInTheResponseBody(String key, String value) {
		SerenityRest.lastResponse().then().assertThat().body(key, equalTo(value));
	}
}
