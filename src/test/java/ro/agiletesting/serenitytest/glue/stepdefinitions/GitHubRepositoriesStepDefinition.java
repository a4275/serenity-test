package ro.agiletesting.serenitytest.glue.stepdefinitions;

import io.cucumber.java.Before;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.serenitybdd.core.environment.EnvironmentSpecificConfiguration;
import net.serenitybdd.rest.SerenityRest;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.actors.OnStage;
import net.serenitybdd.screenplay.actors.OnlineCast;
import net.serenitybdd.screenplay.rest.abilities.CallAnApi;
import net.serenitybdd.screenplay.rest.interactions.Get;
import net.thucydides.core.util.EnvironmentVariables;

import java.util.List;

import static net.serenitybdd.screenplay.rest.questions.ResponseConsequence.seeThatResponse;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class GitHubRepositoriesStepDefinition {
	private static final String APPLICATION_VND_GITHUB_V_3_JSON = "application/vnd.github.v3+json";
	private static final String ACCEPT = "Accept";
	private String theRestApiBaseUrl;
	private EnvironmentVariables environmentVariables;
	private Actor theGitHubUser;
	private String user;

	@Before
	public void configureBaseUrl() {
		theRestApiBaseUrl = EnvironmentSpecificConfiguration.from(environmentVariables)
				.getProperty("restapi.baseurl");
		OnStage.setTheStage(new OnlineCast());
	}

	@Given("the user {string}")
	public void theUser(String user) {
		this.user = user;
		theGitHubUser = Actor.named(user).whoCan(CallAnApi.at(theRestApiBaseUrl));
	}

	@When("he retrieves all his repositories")
	public void heGetsAllRepositories() {
		theGitHubUser.attemptsTo(
				Get.resource("/users/" + user + "/repos")
						.with(request -> request.header(ACCEPT, APPLICATION_VND_GITHUB_V_3_JSON))
		);
	}

	@Then("he should find at least some of the following repositories:")
	public void heShouldFindSomeOfTheFollowing(List<String> repositories) {
		theGitHubUser.should(
				seeThatResponse(
						response -> response.statusCode(200)
								.body("name", hasItems(repositories.toArray(new String[0])))
				)
		);
	}

	@When("he retrieves repository {string} branches")
	public void heRetrievesRepository(String repository) {
		theGitHubUser.attemptsTo(
				Get.resource("/repos/" + user + "/" + repository + "/branches")
						.with(request -> request.header(ACCEPT, APPLICATION_VND_GITHUB_V_3_JSON))
		);
	}

	@Then("he should find at least the following branch {string}")
	public void heShouldFindAtLeastTheFollowingBranch(String branch) {
		theGitHubUser.should(
				seeThatResponse(
						response -> response.statusCode(200)
								.body("name", hasItems(branch))
				)
		);
	}
}
